echo 'Trying to initialize jenkins job'
until curl -fs -XGET 'http://localhost:8080/job/Pipeline/api/json'> /dev/null; do
  echo 'Waiting for Jenkins to start'
  curl -fs -XPOST 'http://localhost:8080/createItem?name=Pipeline' --data-binary @/home/vagrant/demo/infrastructure/vagrant/jenkins_job.xml -H 'Content-Type:text/xml'
  sleep 2
done

