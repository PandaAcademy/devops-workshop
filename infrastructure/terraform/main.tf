provider "aws" {
  region = "us-west-2"
}

resource "aws_key_pair" "panda" {
  key_name   = "panda-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJ0m1DRw7fbkm10VNxUfHi64WHhjGvX8CAX5+Ei5G6rW/dMQNLwHp/q16oIpoFSVHDMxIPTNUtoObHRJXLaZQC5I1V01vBigXnOwTtoCdkAyWiLyFZPe7HPt2UE0PPiZ8YG1b6+hgyKhnz/N30hTrYESC3nNGk1B6OKpJxWRJa4bbkWz9ClnrbyiWYm0JW47IyVpOoFVfEI47DSfvhrUeJ688jk3p/UlVNZr2CuVxTnX8Eks7BUt6Ti6B/D9ribXerFNMtfgcHlSmicrsiKr24Rt7MpyLYsEger5axkMAZioS897Zco8Z/oMCRJHzylLf/RQwIajs8WNg7SKhwDxDZ email@example.com"
}

resource "aws_security_group" "allow_8080_22" {
  name        = "allow_8080_22"
  description = "Allow port 8080 and 22 inbound traffic"

  ingress {
    description = "Allow 8080 from VPC"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow 22 from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "panda" {
  count                  = 1
  ami                    = "ami-003634241a8fcdec0"
  instance_type          = "t2.micro"
  key_name               = "panda-key"
  vpc_security_group_ids = [aws_security_group.allow_8080_22.id]
}

resource "local_file" "ansible_inventory" {
    content     = templatefile("inventory.tpl", { ansible_ip =  aws_instance.panda.0.public_ip })
    filename = "${path.module}/../ansible/inventory"
}

output "PublicIp" {
  value = aws_instance.panda.0.public_ip
}
