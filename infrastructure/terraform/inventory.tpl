[all:vars]
ansible_user=ubuntu
ansible_python_interpreter=/usr/bin/python3
ansible_ssh_private_key_file=./id_rsa
ansible_ssh_common_args='-o StrictHostKeyChecking=no'

[aws]
${ansible_ip}