node ("master") {

  def mavenHome

  stage ("Prerequisites") {
    mavenHome = tool "Maven"

    withCredentials([file(credentialsId: 'settings.xml', variable: 'SETTINGS_PATH')]) {
      sh "cp -f $SETTINGS_PATH ${mavenHome}/conf/settings.xml"
    }
  }

  stage ("Git repository") {
    git branch: "master", url: 'ssh://root@git:22/git/'
  }

  stage ("Build") {
    sh "${mavenHome}/bin/mvn clean install"
  }
  
  stage ("Build docker image") {
    sh "${mavenHome}/bin/mvn install -Pbuild-docker -Dmaven.test.skip=true"
  }

  stage ("Zalenium tests") {
    docker.image('panda-application:latest').withRun('--network=panda_network --name=application -p 18080:8080') 
    { c ->
      dir('./zalenium/') {
        sh "${mavenHome}/bin/mvn clean test -Dwebdriver.type=remote -Dwebdriver.url=http://zalenium:4444/wd/hub -Dwebdriver.cap.browserName=chrome"
      }
    }
  }

  stage ("Send artifact to repository") {
    sh "${mavenHome}/bin/mvn deploy -Dmaven.test.skip=true"
  }

  stage ("Create AWS instance") {
    dir('infrastructure/terraform') {
      sh "terraform init && terraform apply -auto-approve"    
    }
  }

  stage ("Deploy application") {
    dir('infrastructure/ansible') {
      sh "chmod 600 id_rsa && ansible-playbook -i ./inventory deploy_application.yml"
    }
  }
}
