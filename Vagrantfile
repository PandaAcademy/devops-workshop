$set_environment_variables = <<SCRIPT
tee "/etc/profile.d/myvars.sh" > "/dev/null" <<EOF
# AWS environment variables.
export AWS_ACCESS_KEY_ID=#{ENV['AWS_ACCESS_KEY_ID']} 
export AWS_SECRET_ACCESS_KEY=#{ENV['AWS_SECRET_ACCESS_KEY']}
EOF
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 3
  end
  config.vm.box = "bento/ubuntu-18.04"
  config.vm.network "private_network", ip: "192.168.44.44"

  config.vm.provision "shell", inline: $set_environment_variables, run: "always"
  config.vm.provision :docker
  config.vm.provision "shell", inline: <<-EOF
    apt-get update
    apt-get install -y git
    usermod -aG docker vagrant
    chown vagrant:docker /var/run/docker.sock
    rm -rf demo
  EOF

  config.vm.provision "shell", privileged: false, inline: <<-EOF
    git clone https://PandaAcademy@bitbucket.org/PandaAcademy/devops-workshop.git demo
    docker pull elgalu/selenium
    mkdir /home/vagrant/demo/videos
  EOF
  
  config.vm.provision :docker_compose, yml: "/home/vagrant/demo/docker-compose.yml", run: "always"
  
  config.vm.provision "shell", privileged: false, inline: <<-EOT
    timeout 120 /home/vagrant/demo/infrastructure/vagrant/jenkins_job.sh || echo 'Error initializing Jenkins Job'
    sudo cp /home/vagrant/demo/infrastructure/vagrant/id_rsa /home/vagrant/.ssh/
    echo "StrictHostKeyChecking no" >> ~/.ssh/config
    git clone ssh://root@localhost:10022/git/
    /home/vagrant/demo/infrastructure/vagrant/success.sh
  EOT
end
